# Offline first transportation schedule web app for mobile & desktop

### Important
GTFS files are too large to be uploaded to Github, so
please [download all the GTFS files](http://www.stm.info/sites/default/files/gtfs/gtfs_stm.zip) and place them in `src/stm-files` locally to run locally.

### Resources
- [STM Developers - OPEN DATA - GTFS](http://www.stm.info/en/about/developers)
- [The Offline Cookbook](https://jakearchibald.com/2014/offline-cookbook/)

[Source code repo](https://gitlab.com/yhagio/offline-stm)

[DEMO URL](http://yhagio.gitlab.io/offline-stm-production/)