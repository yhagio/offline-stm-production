// function readTextFile(file) {
//     var rawFile = new XMLHttpRequest();
//     rawFile.open("GET", file, false);
//     rawFile.onreadystatechange = function() {
//       if(rawFile.readyState === 4) {
//         if(rawFile.status === 200 || rawFile.status == 0) {
//           var allText = rawFile.responseText;
//           document.getElementById('text-area').innerText = allText; 
//         }
//       }
//     }
//     rawFile.send(null);
// }
// readTextFile('https://gitlab.com/yhagio/stm-files/raw/master/agency.txt');

$(document).ready(function(){

  var URL = '/offline-stm-production/stm-files/agency.txt';
  
  $.ajax({
    url: URL,
    method: 'GET',
    dataType: 'text',
    success: function(data) {
      console.log('SUC: ', data);
      $('#text-area').html(data);
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('ERR: ', jqXHR);
    }
  });

});


// var URL = '../stm-files/agency.txt';

// fetch(URL)
//   .then(function(res) {
//     console.log(res.json());
//   }).
//   catch(function(error) {
//     console.log('ERROR: ', error);
//   });



// function textFileToJSON(textFile) {
//   let lines=textFile.split("\n");
//   let result = [];
//   let headers=lines[0].split(",");
//   for(let i=1;i<lines.length;i++){
//     let obj = {};
//     let currentline=lines[i].split(",");

//     for(let j=0;j<headers.length;j++){
//       obj[headers[j]] = currentline[j];
//     }
//     result.push(obj);
//   }
//   return result;
// }

//make sure that Service Workers are supported.
if (navigator.serviceWorker) {
  navigator.serviceWorker.register('/sw.js', {scope: '/'})
    .then(function (registration) {
      console.log('Registered service worker');
    })
    .catch(function (e) {
      console.error('Error on registering service worker:\n', e);
    });
} else {
  console.log('Service Worker is not supported in this browser.');
}